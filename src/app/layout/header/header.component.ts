import {Component, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Output() public title: string = 'Zonky app';

  constructor() {
  }

  ngOnInit() {
  }

}
