import {Component, OnInit, Output} from '@angular/core';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['footer.component.scss']
})
export class FooterComponent implements OnInit {

  @Output() public author = "Zdeněk Smejkal";

  ngOnInit() {

  }
}
