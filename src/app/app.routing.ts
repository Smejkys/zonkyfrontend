import {Routes, RouterModule} from '@angular/router';
import {MarketplaceComponent} from './loans/components/marketplace/Marketplace.component';

const appRoutes: Routes = [
  {
    path: '',
    data: {pageTitle: 'application.long_name'},
    children: [
      {
        path: '',
        component: MarketplaceComponent
      },
      {
        path: ':selectMarketplace',
        component: MarketplaceComponent
      },
      {path: '**', redirectTo: ''}
    ]
  }
];

export const routing = RouterModule.forRoot(appRoutes);
