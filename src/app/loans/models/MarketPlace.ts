import {RatingEnum} from './RatingEnum';

export interface Marketplace {
  key: RatingEnum;
  value: string;
}

export const marketplaces: Marketplace[] = [
  {key: RatingEnum.AAAAAA, value: '2.99%'},
  {key: RatingEnum.AAAAA, value: '3.99%'},
  {key: RatingEnum.AAAA, value: '4.99%'},
  {key: RatingEnum.AAA, value: '5.99%'},
  {key: RatingEnum.AAE, value: '6.99%'},
  {key: RatingEnum.AA, value: '8.49%'},
  {key: RatingEnum.AE, value: '9.49%'},
  {key: RatingEnum.A, value: '10.99%'},
  {key: RatingEnum.B, value: '13.49%'},
  {key: RatingEnum.C, value: '15.49%'},
  {key: RatingEnum.D, value: '19.99%'}
];
