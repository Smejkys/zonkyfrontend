import {Photos} from './Photos';

export class Market {

  id: number;
  url: string;
  name: string;
  story: string;
  purpose: string;
  photos: Photos[];
  userId: number;
  nickName: string;
  termInMonths: number;
  interestRate: number;
  revenueRate: number;
  annuity: number;
  premium: {};
  rating: string;
  topped: boolean;
  amount: number;
  remainingInvestment: number;
  investmentRate: number;
  covered: boolean;

  constructor() {
  }
}
