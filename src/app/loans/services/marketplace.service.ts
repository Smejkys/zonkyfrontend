import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Market} from '../models/Market';
import {Marketplace, marketplaces} from '../models/MarketPlace';
import {RatingEnum} from '../models/RatingEnum';

@Injectable()
export class MarketplaceService {
  public updating$ = new BehaviorSubject<boolean>(false);
  public marketplaces$ = new BehaviorSubject<Market[]>(null);
  public averagePrice$ = new BehaviorSubject<number>(null);

  isUpdating$() {
    return this.updating$.asObservable();
  }

  setUpdating(isUpdating: boolean) {
    this.updating$.next(isUpdating);
  }

  getMarketplaces$() {
    return this.marketplaces$.asObservable();
  }

  setMarketplaces(marketplaces: Market[]) {
    this.marketplaces$.next(marketplaces);
  }


  allTypeMarketplaces(): Marketplace[]{
    return marketplaces;
  }

  /**
   * Zprumeruje vysi pujcek
   * @param marketplaces: Market[]
   */
  averagePriceMarketplace(marketplaces: Market[]) {
    let amountPrice: number = 0, averagePrice: number;

    if (marketplaces.length > 0) {
      for (let marketplace of marketplaces) {
        amountPrice += marketplace.amount;
      }
      averagePrice = amountPrice / marketplaces.length;
    }

    this.averagePrice$.next(averagePrice);
    this.setUpdating(false);
  }

  getAveragePriceOfMarket$(): Observable<number> {
    return this.averagePrice$.asObservable();
  }

  /**
   * Vyselektuje rating podle klice
   * @param selectMarketplace
   */
  getMarketplaceValue(selectMarketplace: RatingEnum): Marketplace {
    let markets = this.allTypeMarketplaces();
    let elementPos = markets.map(function (x: Marketplace) {
      return x.key;
    }).indexOf(selectMarketplace);

    if (elementPos == null) {
      return null;
    } else {
      return markets[elementPos];
    }
  }

  formattingCzechCurrency(averagePriceMarket: number): string {
    if (averagePriceMarket == null) {
      return null;
    }

    let formattingCurency = new Intl.NumberFormat('cs-CZ', {
      style: 'currency',
      currency: 'CZK',
      minimumFractionDigits: 0,
      maximumFractionDigits: 0,
    }).format(averagePriceMarket);

    return formattingCurency;
  }

}
