/**
 * Testing Marketplace.service
 */
import {MarketplaceService} from './Marketplace.service';
import {Market} from '../models/Market';
import {RatingEnum} from '../models/RatingEnum';


describe('MarketplaceService without Angular testing support', () => {
  let marketplaceService: MarketplaceService;

  beforeEach(() => {
    marketplaceService = new MarketplaceService();
  });


  it('#getValue should return default value', async () => {

    marketplaceService = new MarketplaceService();

    marketplaceService.updating$.subscribe((users: boolean) => {
      expect(users).toBe(false);
    });

    marketplaceService.marketplaces$.subscribe((markets: Market[]) => {
      expect(markets).toBeNull();
    });

    marketplaceService.averagePrice$.subscribe((averagePrice: number) => {
      expect(averagePrice).toBeNull();
    });

  });

  it('#formattingCzechCurrency should formatting price to CZK format', () => {
    expect(marketplaceService.formattingCzechCurrency(450)).toBe('450\xa0Kč');
  });

  it('#formattingCzechCurrency should be null', () => {
    expect(marketplaceService.formattingCzechCurrency(null)).toBeNull();
  });

  it('#isUpdating should set updating on True', async () => {
    marketplaceService.setUpdating(true);
    expect(marketplaceService.isUpdating$()).toBeTruthy();
  });

  it('#getMarketplaces$ should default set Null', async () => {
    marketplaceService.getMarketplaces$().subscribe((o: Market[]) => expect(o).toBeNull());
  });

  it('#averagePriceMarketplace should default return null', async () => {
    marketplaceService.getMarketplaces$().subscribe((o: Market[]) => expect(o).toBeNull());
  });

  it('#averagePriceMarketplace should create Market in a array Market', async () => {
    let markets: Market[] = [];
    markets.push(new Market());

    marketplaceService.setMarketplaces(markets);
    marketplaceService.getMarketplaces$().subscribe((o: Market[]) => expect(o.length).toBeGreaterThanOrEqual(1));

  });

  it('#getMarketplaceValue should be defined MarketPlace by enum key', () => {
    expect(marketplaceService.getMarketplaceValue(RatingEnum.AAAAA)).toBeDefined();
  });

  it('#getMarketplaceValue should be undefined MarketPlace by null key', () => {
    expect(marketplaceService.getMarketplaceValue(null)).toBeUndefined();
  });

  it('#getAveragePriceOfMarket$ should calculating average marketplace', () => {
    let markets: Market[] = [];

    let market1: Market = new Market();
    market1.amount = 500;
    markets.push(market1);

    let market2: Market = new Market();
    market2.amount = 1000;
    markets.push(market2);

    marketplaceService.averagePriceMarketplace(markets);
    marketplaceService.getAveragePriceOfMarket$().subscribe((o: number) => expect(o).toBe(750));
  });

});
