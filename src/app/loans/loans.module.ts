import {NgModule} from '@angular/core';

import {HAMMER_GESTURE_CONFIG} from '@angular/platform-browser';

import {MarketplaceComponent} from './components/marketplace/Marketplace.component';
import {MaterialModule} from '../layout/material.module';
import {RouterModule} from '@angular/router';
import {GestureConfig} from '@angular/material/core';
import {LoansFacade} from './components/loans.facade';
import {MarketplaceApi} from './api/marketplace.api';
import {MarketplaceService} from './services/marketplace.service';

@NgModule({
  exports: [],
  declarations: [
    MarketplaceComponent
  ],
  imports: [
    MaterialModule,
    RouterModule
  ],
  providers: [
    {provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig},
    LoansFacade,
    MarketplaceApi,
    MarketplaceService
  ]
})
export class LoansModule {
}
