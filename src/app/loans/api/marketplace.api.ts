import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Market} from '../models/Market';

@Injectable()
export class MarketplaceApi {

  readonly API = '/loans/marketplace';

  constructor(private http: HttpClient) {}


  /**
   * Vrati zaznamy ruzneho typu
   */
  getMarketplaces(): Observable<Market[]> {
    return this.http.get<Market[]>(this.API);
  }

  searchMarketplaces(search:string): Observable<Market[]>{
    let headers = new HttpHeaders();
    headers = headers.append('Accept', 'application/json');
    headers = headers.append("content-type", "application/json");
    return this.http.get<Market[]>(`${this.API}/?rating__eq=${search}`,{ headers: headers});
  }

}
