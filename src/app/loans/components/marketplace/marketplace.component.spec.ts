import {TestBed, async, ComponentFixture} from '@angular/core/testing';
import {MarketplaceComponent} from './marketplace.component';
import {MaterialModule} from '../../../layout/material.module';
import {MarketplaceService} from '../../services/marketplace.service';
import {BrowserModule} from '@angular/platform-browser';
import {routing} from '../../../app.routing';
import {LoansModule} from '../../loans.module';
import {LayoutModule} from '../../../layout/layout.module';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {of} from 'rxjs';
import {APP_BASE_HREF} from '@angular/common';
import {LOCALE_ID} from '@angular/core';

describe('MarketplaceComponent', () => {
  let component: MarketplaceComponent;
  let fixture: ComponentFixture<MarketplaceComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        MarketplaceComponent
      ],
      imports: [
        routing,
        MaterialModule,
        BrowserModule,
        LoansModule,
        LayoutModule,
        HttpClientModule,
        BrowserAnimationsModule
      ], providers: [{provide: APP_BASE_HREF, useValue: '/'},{
        provide: LOCALE_ID,
        useValue: 'cs-CZ' // 'de-DE' for Germany, 'fr-FR' for France ...
      },]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarketplaceComponent);
    component = fixture.debugElement.componentInstance;
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should use the isUpdating$ from the service', () => {
    const marketplaceService = fixture.debugElement.injector.get(MarketplaceService);
    fixture.detectChanges();
    expect(marketplaceService.isUpdating$()).toEqual(component.isUpdating$);
  });

  it('should use the averagePrice$ from the service', () => {
    const marketplaceService = fixture.debugElement.injector.get(MarketplaceService);
    fixture.detectChanges();
    expect(marketplaceService.getAveragePriceOfMarket$()).toEqual(component.averagePrice$);
  });

  it('should use the allTypeMarketplaces from the service', () => {
    const marketplaceService = fixture.debugElement.injector.get(MarketplaceService);
    fixture.detectChanges();
    expect(marketplaceService.allTypeMarketplaces()).toEqual(component.allTypeMarketplaces);
  });

  it('should show mat-spinner icon', async() => {
    component.isUpdating$ = of(true);

    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.innerHTML).toBeDefined('mat-spinner');
  });

  it('should show average formatting price',async () => {
    component.averagePrice$ = of(250000);

    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.average-market').textContent).toContain('250\xa0000\xa0Kč');
  });


});
