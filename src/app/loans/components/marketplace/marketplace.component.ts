import {Component, OnInit, Output} from '@angular/core';
import {LoansFacade} from '../loans.facade';
import {ActivatedRoute} from '@angular/router';
import {Marketplace} from '../../models/MarketPlace';
import {Observable} from 'rxjs';
import {RatingEnum} from '../../models/RatingEnum';

@Component({
  selector: 'marketplace',
  templateUrl: './marketplace.component.html',
  styleUrls: ['./marketplace.component.scss']
})
export class MarketplaceComponent implements OnInit {
  @Output() public allTypeMarketplaces: Marketplace[] = [];
  @Output() public marketplaceValue: Marketplace;
  @Output() public breakpoint: number;

  @Output() public isUpdating$: Observable<boolean>;
  @Output() public averagePrice$: Observable<number>;
  @Output() public czechCurrency: string;

  constructor(private loansFacade: LoansFacade, private route: ActivatedRoute) {
    this.isUpdating$ = this.loansFacade.isUpdating$();
    this.averagePrice$ = this.loansFacade.getAveragePriceOfMarket$();
    this.allTypeMarketplaces = this.loansFacade.allTypeMarketplaces();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      let selectMarketplace: RatingEnum = params['selectMarketplace'];
      if (selectMarketplace != null) {
        this.loansFacade.averagePriceMarketplace(selectMarketplace);
        this.marketplaceValue = this.loansFacade.getMarketplaceValue(selectMarketplace);
      }
    });

    this.breakpoint = (window.innerWidth <= 700) ? 6 : this.allTypeMarketplaces.length;
  }

  onResize(event) {
    this.breakpoint = (event.target.innerWidth <= 700) ? 6 : 12;
  }

  formattingToCzechCurrency(averagePrice: number): string {
    return this.loansFacade.formattingCzechCurrency(averagePrice);
  }

}
