import {Injectable} from '@angular/core';
import {MarketplaceApi} from '../api/marketplace.api';
import {Observable} from 'rxjs';
import {Market} from '../models/Market';
import {tap} from 'rxjs/operators';
import {MarketplaceService} from '../services/marketplace.service';
import {Marketplace} from '../models/MarketPlace';
import {RatingEnum} from '../models/RatingEnum';

@Injectable()
export class LoansFacade {

  constructor(private marketplaceApi: MarketplaceApi,
              private marketplaceService: MarketplaceService) {
  }

  getMarketplaces$(): Observable<Market[]> {
    return this.marketplaceApi.getMarketplaces().pipe(tap(marketplaces => this.marketplaceService.setMarketplaces(marketplaces)));
  }

  averagePriceMarketplace(search: string) {
    this.marketplaceService.setUpdating(true);
    this.marketplaceApi.searchMarketplaces(search).subscribe(
      (markets: Market[]) => {
        this.marketplaceService.averagePriceMarketplace(markets);
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  getMarketplaceValue(selectMarketplace: RatingEnum): Marketplace {
    return this.marketplaceService.getMarketplaceValue(selectMarketplace);
  }

  allTypeMarketplaces(): Marketplace[] {
    return this.marketplaceService.allTypeMarketplaces();
  }

  formattingCzechCurrency(averagePriceMarket: number): string{
    return this.marketplaceService.formattingCzechCurrency(averagePriceMarket);
  }

  isUpdating$(): Observable<boolean> {
    return this.marketplaceService.isUpdating$();
  }

  getAveragePriceOfMarket$(): Observable<number> {
    return this.marketplaceService.getAveragePriceOfMarket$();
  }

}
